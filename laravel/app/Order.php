<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['product_id', 'quantity', 'price', 'date_add'];
    public $timestamps = false;
}
