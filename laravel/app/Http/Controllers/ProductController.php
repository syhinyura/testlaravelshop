<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::join('categories', 'products.category_id', '=', 'categories.id')
        ->select('products.*', 'categories.name as category')
        ->orderBy('id','desc')
        ->get();
        return view('product', ['products' => $products]);
    }
    public function save(Request $request)
    {
        $messages = [
            'name.required' => 'Field "Name" is required!',
            'category.required' => 'Field "Category" is required!',
            'price.numeric' => 'Field "Price" is numeric!',
        ];
        $rules = [
            'name' => 'required',
            'price' => 'numeric',
            'category' => 'required',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category;
        $result = $product->save();
        if ($result) {
            $notification = array(
                'message'=>'Product Save Successfully!',
                'type'=>'success'
            );
            return Redirect()->back()->with($notification);

        }else{
            $notification = array(
                'message'=>'Some Error!',
                'type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }

    public function delete(Request $request){
        $id  = $request->id;
        if($id){
            Product::destroy($id);
        }
    }

    public function update(Request $request, $id){
        $messages = [
            'name.required' => 'Field "Name" is required!',
            'category.required' => 'Field "Category" is required!',
            'price.numeric' => 'Field "Price" is numeric!',
        ];
        $rules = [
            'name' => 'required',
            'price' => 'numeric',
            'category' => 'required',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $product = Product::find($id);
        if($product){
            $product->name = $request->name;
            $product->price = $request->price;
            $product->category_id = $request->category;
            $result = $product->save();
            if ($result) {
                $notification=array(
                    'message'=>'Product Update Successfully!',
                    'type'=>'success'
                );
                return Redirect()->back()->with($notification);
            }else{
                $notification=array(
                    'message'=>'Some Error!',
                    'type'=>'error'
                );
                return Redirect()->back()->with($notification);
            }
        }
    }
}
