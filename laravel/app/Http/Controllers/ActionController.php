<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ActionController extends Controller
{
    public function filterProductsFromCategory(Request $request)
    {
        $category = $request->input('category');
        if($category){
            echo Product::where('category_id', $category)->get()->toJson();
        }
    }
    public function createProduct(Request $request)
    {
        $categories = Category::all();
        return view('ajax.createProduct', ['categories' => $categories]);
    }
    public function updateProduct( $id)
    {
        $product = Product::where('id', $id)->get()->toArray();
        $categories = Category::all();
        return view('ajax.updateProduct', ['product' => $product, 'categories' => $categories]);
    }

    public function createCategory(Request $request)
    {
        return view('ajax.createCategory');
    }
    public function updateCategory( $id)
    {
        $category = Category::where('id', $id)->get()->toArray();
        return view('ajax.updateCategory', ['category' => $category]);
    }

}
