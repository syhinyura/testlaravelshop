<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('id','desc')->get();
        return view('category', ['categories' => $categories]);
    }

    public function save(Request $request)
    {
        $messages = [
            'name.required' => 'Field "Name" is required!'
        ];
        $rules = [
            'name' => 'required'
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $category = new Category();
        $category->name = $request->name;
        $result = $category->save();
        if ($result) {
            $notification=array(
                'message'=>'Category Save Successfully!',
                'type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification=array(
                'message'=>'Some Error!',
                'type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }

    public function delete(Request $request){
        $id  = $request->id;
        if($id){
            $result = Category::destroy($id);
            if ($result) {
                $notification=array(
                    'messege'=>'Category Delete Successfully',
                    'alert-type'=>'success'
                );
                return Redirect()->back()->with($notification);
            }else{
                return Redirect()->back();
            }
        }
    }

    public function update(Request $request, $id){
        $messages = [
            'name.required' => 'Field "Name" is required!'
        ];
        $rules = [
            'name' => 'required'
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $category = Category::find($id);
        if($category){
            $category->name = $request->name;
            $result = $category->save();
            if ($result) {
                $notification=array(
                    'message'=>'Category Update Successfully!',
                    'type'=>'success'
                );
                return Redirect()->back()->with($notification);
            }else{
                $notification=array(
                    'message'=>'Some Error!',
                    'type'=>'error'
                );
                return Redirect()->back()->with($notification);
            }
        }
    }
}
