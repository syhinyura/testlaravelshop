<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index(Request $request){
        $categories = Category::all();
        $to = $from = false;
        if($request->order_from){
            $from = date('Y-m-d', strtotime($request->order_from));
        }
        if($request->order_to){
            $to = date('Y-m-d', strtotime($request->order_to));
        }

        $orders = Order::query();
        $orders->where(function($orders) use ($from, $to ){
            if($from)
                $orders->where('orders.date_add', '>=', $from.' 00:00:00');
            if($to)
                $orders->where('orders.date_add', '<=', $to.' 23:59:59');
        });

        if($request->category){
            //Group by Items
            $result = $orders->where('categories.id', $request->category)
                ->join('products', 'products.id', '=', 'orders.product_id')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->select('orders.*', 'products.name as product_name','categories.name as category_name', 'products.id as product_id')
                ->get();
            $orders = $result->toArray();
            $orders = group_by_key('product_id', $orders);
            return view('orders_by_product', ['ordersGroups' => $orders, 'categories' => $categories ]);
        }else{
            //Group by Category
            $result = $orders->join('products', 'products.id', '=', 'orders.product_id')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->select('orders.*', 'products.name as product_name','categories.name as category_name', 'categories.id as category_id')
                ->get();
            $orders = $result->toArray();
            $orders = group_by_key('category_id', $orders);
            return view('orders', ['ordersGroups' => $orders, 'categories' => $categories ]);
        }
    }

    public function create(Request $request){
        $messages = [
            'category.required' => 'Field "Category" is required!',
            'product.required' => 'Field "Product" is required!',
            'quantity.integer' => 'Field "Quantity" is integer!',
        ];
        $rules = [
            'category' => 'required|exists:categories,id',
            'product' => 'required|exists:products,id',
            'quantity' => 'integer',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $product = Product::find($request->product)->toArray();
        $order = new Order();
        $order->product_id = $request->product;
        $order->quantity = $request->quantity;
        $order->price = $product['price']*$request->quantity;
        $order->date_add = date("Y-m-d H:i:s", time());
        $result = $order->save();
        if ($result) {
            $notification=array(
                'message'=>'Order Add Successfully!',
                'type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
            return Redirect()->back();
        }

    }
}
