@extends('layouts.template')
@section('title', 'Home')
@section('content')
    @include('components.notification')
    <div class="container">
        <form action="{{url('createOrder')}}" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="categorySelect">Category</label>
                    <select class="form-control" id="categorySelect" name="category" data_action="filterProductsFromCategory">
                        <option></option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="productSelect">Product</label>
                    <select class="form-control" id="productSelect" name="product">
                        <option></option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="quantityInput">Quantity</label>
                    <input type="text" class="form-control" id="quantityInput" placeholder="Quantity" name="quantity">
                </div>
            </div>
            <div class="modal-footer custom">
                <button type="submit" class="btn btn-primary">Buy</button>
            </div>
        </form>
    </div>
@endsection

