@extends('layouts.template')
@section('title', 'Categories')
@section('content')
    @include('components.notification')
    <div class="row mt-3">
        <h4 class="mt-3">Category List</h4>
        <button class="create_button" data-fancybox data-type="ajax" data-src="/action/createCategory/">+Create New</button>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category['id']}}</td>
                    <td>{{$category['name']}}</td>
                    <td>
                        <i class="fa fa-pencil update_button" aria-hidden="true" title="Update" data-fancybox data-type="ajax" data-src="/action/updateCategory/{{$category['id']}}"></i>
                        <i class="fa fa-trash delete_button" data_type="category" data_value="{{$category['id']}}" data_token="{{ csrf_token() }}" aria-hidden="true" title="Delete"></i>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
