@if(!empty($errors->all()))
    <p></p>
    @foreach($errors->all() as $message)
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endforeach
@endif
@if ('success' == Session::get('type'))
    <p></p>
    <div class="alert alert-success alert-block mt-3">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ Session::get('message') }}</strong>
    </div>
@endif
@if ('error' == Session::get('type'))
    <p></p>
    <div class="alert alert-danger alert-block mt-3">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ Session::get('message') }}</strong>
    </div>
@endif
