<form action="{{url('orders')}}" method="get">
    <input type="hidden" name='filter' value="Y">
    <div class="row">
        <div class="col-md-2 mb-3">
            <label for="order_from">Order from:</label>
            <input
                type="text"
                class="form-control"
                id="order_from"
                name="order_from"
                autocomplete="off"
                @if(app('request')->input('order_from'))
                    data_value="{{ date('d/m/Y',strtotime(app('request')->input('order_from'))) }}"
                @endif
            >
        </div>
        <div class="col-md-2">
            <label for="order_to">Order to:</label>
            <input
                type="text"
                class="form-control"
                id="order_to"
                name="order_to"
                autocomplete="off"
                @if(app('request')->input('order_to'))
                     data_value="{{ date('d/m/Y',strtotime(app('request')->input('order_to'))) }}"
                @endif
            >
        </div>
        <div class="col-md-4">
            <label for="category">Category:</label>
            <select class="form-control" id="categorySelect" name="category" data_action="filterProductsFromCategory">
                <option></option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" {{ $category->id == app('request')->input('category') ? 'selected' : '' }}>{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <button class="btn btn-primary form-control filter_button" type="submit">Filter</button>
        </div>
        <div class="col-md-2">
            <button
                class="btn btn-danger form-control filter_button"
                onclick="location.href = location.pathname;return false;">Reset</button>
        </div>
    </div>
</form>
