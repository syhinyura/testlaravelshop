@extends('layouts.template')
@section('title', 'Product')
@section('content')
    @include('components.notification')
    <div class="row mt-3">
        <h4 class="mt-3">Product List</h4>
        <button class="create_button" data-fancybox data-type="ajax" data-src="/action/createProduct/">+Create New</button>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Category</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product['id']}}</td>
                        <td>{{$product['name']}}</td>
                        <td>{{$product['price']}}</td>
                        <td>{{$product['category']}}</td>
                        <td>
                            <i class="fa fa-pencil update_button" aria-hidden="true" title="Update" data-fancybox data-type="ajax" data-src="/action/updateProduct/{{$product['id']}}"></i>
                            <i class="fa fa-trash delete_button" data_type='product' data_value="{{$product['id']}}" data_token="{{ csrf_token() }}" aria-hidden="true" title="Delete"></i>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

