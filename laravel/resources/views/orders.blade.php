@extends('layouts.template')
@section('title', 'Orders')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="mt-3">Order Stats</h4>
            @include('components.filter_orders')
            @foreach($ordersGroups  as $ordersGroup)
            <div class="row">
                <h4 class="mt-3">{{reset($ordersGroup)['category_name']}}</h4>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Time</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ordersGroup  as $orders)
                        <tr>
                            <td>{{$orders['id']}}</td>
                            <td>{{$orders['date_add']}}</td>
                            <td>{{$orders['product_name']}}</td>
                            <td>{{$orders['quantity']}}</td>
                            <td>{{$orders['price']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endforeach
        </div>
    </div>
@endsection
