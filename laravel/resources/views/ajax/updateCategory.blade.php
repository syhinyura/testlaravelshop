<form id="updateCategory" class="fancy_popup_custom" action="{{url('/categories/update/'.$category['0']['id'])}}" method="post">
    @csrf
    <h2 class="mb-3">Update Category</h2>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" value="{{$category[0]['name']}}">
    </div>
    <p class="mb-0 text-right">
        <input type="submit" class="btn btn-primary" value="Update">
    </p>
</form>
