<form id="createCategories" class="fancy_popup_custom" action="{{url('/categories/save')}}" method="post">
    @csrf
    <h2 class="mb-3">Create Category</h2>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>
    <p class="mb-0 text-right">
        <input type="submit" class="btn btn-primary" value="Create">
    </p>
</form>
