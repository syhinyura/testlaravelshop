<form id="updateProduct" class="fancy_popup_custom" action="{{url('/products/update/'.$product['0']['id'])}}" method="post">
    @csrf
    <h2 class="mb-3">Update Product</h2>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" value="{{$product[0]['name']}}">
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input type="text" class="form-control" id="price" name="price" value="{{$product[0]['price']}}">
    </div>
    <div class="form-group">
        <label for="category">Category</label>
        <select class="form-control" id="category" name="category">
            <option></option>
            @foreach($categories as $category)
                <option
                    {{ $category['id'] ==  $product[0]['category_id']? 'selected' : '' }}
                    value="{{$category->id}}" >{{$category->name}}</option>
            @endforeach
        </select>
    </div>
    <p class="mb-0 text-right">
        <input type="submit" class="btn btn-primary" value="Update">
    </p>
</form>
