<form id="createProduct" class="fancy_popup_custom" action="{{url('/products/save')}}" method="post">
    @csrf
    <h2 class="mb-3">Create Product</h2>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input type="text" class="form-control" id="price" name="price">
    </div>
    <div class="form-group">
        <label for="category">Category</label>
        <select class="form-control" id="category" name="category">
            <option></option>
            @foreach($categories as $category)
                <option
                    value="{{$category->id}}" >{{$category->name}}</option>
            @endforeach
        </select>
    </div>
    <p class="mb-0 text-right">
        <input type="submit" class="btn btn-primary" value="Create">
    </p>
</form>
