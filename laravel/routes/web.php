<?php

/*Pages*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/orders/', 'OrderController@index')->name('orders');
Route::get('/products/', 'ProductController@index')->name('products');
Route::get('/categories/', 'CategoryController@index')->name('categories');

/*Products*/
Route::post('/products/save', 'ProductController@save');
Route::delete('/products/delete', 'ProductController@delete');
Route::post('/products/update/{id}', 'ProductController@update');

/*Categories*/
Route::post('/categories/save', 'CategoryController@save');
Route::delete('/categories/delete', 'CategoryController@delete');
Route::post('/categories/update/{id}', 'CategoryController@update');

/*Order*/
Route::post('/createOrder/','OrderController@create');

/*Action*/
Route::get('/action/filterProductsFromCategory', 'ActionController@filterProductsFromCategory');
Route::get('/action/createProduct/', 'ActionController@createProduct');
Route::get('/action/createCategory/', 'ActionController@createCategory');
Route::get('/action/updateProduct/{id}', 'ActionController@updateProduct');
Route::get('/action/updateCategory/{id}', 'ActionController@updateCategory');
