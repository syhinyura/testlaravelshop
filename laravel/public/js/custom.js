$(function () {
    filterProductsFromCategory();
    deleteProduct();
    deleteCategory();
    datePickerInitialize();
});

function filterProductsFromCategory(){
    $('[data_action=filterProductsFromCategory]').on('change', function(){
        let self = this;
        let category = $(self).val();
        if(category) {
            $.ajax({
                type: 'GET',
                url: '/action/filterProductsFromCategory?category=' + category,
                success: function (data) {
                    $('#productSelect').html('');
                    $('#productSelect').append($('<option></option>'));
                    if(!!data){
                        data = JSON.parse(data);
                        data.forEach(function (element) {
                            $('#productSelect').append($('<option value="'+ element['id'] +'">'+ element['name'] +'</option>'));
                        });
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
}

function deleteProduct(){
    $('.delete_button[data_type=product]').on('click', function() {
        let self = this;
        let id = $(self).attr('data_value');
        let token = $(self).attr('data_token');
        if(id) {
            $.ajax({
                type: 'POST',
                url: '/products/delete',
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function (data) {
                    $(self).parents('tr').fadeOut(300, function(){$(this).remove();});
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
}

function deleteCategory(){
    $('.delete_button[data_type=category]').on('click', function() {
        let self = this;
        let id = $(self).attr('data_value');
        let token = $(self).attr('data_token');
        if(id) {
            $.ajax({
                type: 'POST',
                url: '/categories/delete',
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function (data) {
                    $(self).parents('tr').fadeOut(300, function(){$(this).remove();});
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
}

function datePickerInitialize(){
    $('#order_from, #order_to').datepicker({
        format: 'dd.mm.yyyy',
    });
    if($("#order_from").attr('data_value'))
        $('#order_from').datepicker("setDate", $("#order_from").attr('data_value'));
    if($("#order_to").attr('data_value'))
         $('#order_to').datepicker("setDate", $("#order_to").attr('data_value'));
}
